# Lab5 -- Integration testing


## Introduction

We've covered unit testing, which is widely used in indusry and is completely whitebox, but there are times when you need to use blackbox testing, for example if you cannot access code module, or if you need to check propper work of module, while calling it locally, so it returns correct values. That's what integration testing is being used for, it can be used both for whitebox and blackbox testing, depending on your goals and possibilities.   ***Let's roll!***🚀️

## Integration testing

Well, if unit testing is all about atomicity and independency, integration testing doesn't think like it, it is used less often then unit testing because it requires more resourses, and I haven't met a team yet, where developers were doing integration tests, usually it is a task for the automated testing team, to check integration of one module with another. Main goal of integration testing is to check that two connected modules(or more, but usually two is enough, because it is less complicated to code and maintain)are working propperly.

## BVA

BVA is a top method for creating integration tests and blackbox tests, when using BVA you should take module(or few modules) and analyse inputs and theirs boudaries, and then pick values that are placed on the border of equivalence classes. To assure the correctness it is usually to chek one value in between of the boundaries just in case.


## Lab
Key for spring semester in 2022 is `AKfycby4bekTEgvzyqqCKaK8Fd5DO_R-6UIA7IIIpwq59iSOniYZxnV9VNYwK4q84et1j9B38w`
Innopolis email is k.rizatdinov@innopolis.university

### Specs 
```
Here is InnoCar Specs:
Budet car price per minute = 21
Luxury car price per minute = 42
Fixed price per km = 17
Allowed deviations in % = 13
Inno discount in % = 10
```

### BVA table
| Parameter          | Equivalence Classes             |
|--------------------|---------------------------------|
| type               | budget, luxury, nonsense        |
| plan               | fixed_price, minute, nonsense    |
| distance           | <=0, >0                         |
| planned_distance   | <=0, >0                         |
| time               | <=0, >0                         |
| planned_time       | <=0, >0                         |
| inno_discount      | yes, no, nonsense               |


### Decision Table
| Conditions (inputs) | Values                        | R1  |  R2   | R3  |  R4  |    R5    |    R6    |    R7    |     R8      |   R9   |  R10   |  R11   |  R12   |     R13     |     R14     |
|---------------------|-------------------------------|-----|-------|-----|------|----------|----------|----------|-------------|--------|--------|--------|--------|-------------|-------------|
| type                | budget, luxury, nonsense      |  *  |   *   |  *  |  *   | nonsense |    *     |    *     |   luxury    | luxury | budget | luxury | budget |   budget    |   budget    |
| plan                | minute, fixed_price, nonsense  |  *  |   *   |  *  |  *   |    *     | nonsense |    *     | fixed_price  | minute | minute | minute | minute | fixed_price  | fixed_price  |
| distance            | <=0, >0                       | <=0 |   >0  |  >0 |  >0  |    >0    |    >0    |    >0    |      >0     |   >0   |   >0   |   >0   |   >0   |     >0      |     >0      |
| planned_distance    | <=0, >0                       |  *  |  <=0  |  >0 |  >0  |    >0    |    >0    |    >0    |      >0     |   >0   |   >0   |   >0   |   >0   |     >0      |     >0      |
| time                | <=0, >0                       |  *  |   *   | <=0 |  >0  |    >0    |    >0    |    >0    |      >0     |   >0   |   >0   |   >0   |   >0   |     >0      |     >0      |
| planned_time        | <=0, >0                       |  *  |   *   |  *  | <=0  |    >0    |    >0    |    >0    |      >0     |   >0   |   >0   |   >0   |   >0   |     >0      |     >0      |
| inno_discount       | yes, no, nonsense             |  *  |   *   |  *  |  *   |    *     |    *     | nonsense |      yes    |  yes   |   yes  |   no   |  no    |     yes     |     no      |
| 200                 | 200                           |     |       |     |      |          |          |          |             |   X    |   X    |   X    |   X    |      X      |      X      |
| Invalid Request     | Invalid Request               |  X  |   X   |  X  |  X   |    X     |    X     |    X     |      X      |        |        |        |        |             |             |


### Test Cases
| ID  | Desition Table Entry | distance | planned_distance | time | planned_time | type     | plan        | inno_discount | Expected Result | Actual Result   |
|-----|----------------------|----------|------------------|------|--------------|----------|-------------|---------------|-----------------|-----------------|
|  1  |          R1          |    0     |         1        |  1   |       1      |  budget  |    minute   |      yes      | Invalid Request |       14.7      |
|  2  |          R1          |    -1    |         1        |  1   |       1      |  budget  |    minute   |      yes      | Invalid Request | Invalid Request |
|  3  |          R2          |    1     |         0        |  1   |       1      |  budget  |    minute   |      yes      | Invalid Request |       14.7      |
|  4  |          R2          |    1     |         -1       |  1   |       1      |  budget  |    minute   |      yes      | Invalid Request | Invalid Request |
|  5  |          R3          |    1     |         1        |  0   |       1      |  budget  |    minute   |      yes      | Invalid Request |        0        |
|  6  |          R3          |    1     |         1        |  -1  |       1      |  budget  |    minute   |      yes      | Invalid Request | Invalid Request |
|  7  |          R4          |    1     |         1        |  1   |       0      |  budget  |    minute   |      yes      | Invalid Request |       14.7      |
|  8  |          R4          |    1     |         1        |  1   |       -1     |  budget  |    minute   |      yes      | Invalid Request |       14.7      |
|  9  |          R5          |    1     |         1        |  1   |       1      | nonsense |    minute   |      yes      | Invalid Request | Invalid Request |
|  10 |          R6          |    1     |         1        |  1   |       1      |  budget  |   nonsense  |      yes      | Invalid Request | Invalid Request |
|  10 |          R7          |    1     |         1        |  1   |       1      |  budget  |    minute   |    nonsense   | Invalid Request | Invalid Request |
|  11 |          R8          |    1     |         1        |  1   |       1      |  luxury  |  fixed_price |      yes      | Invalid Request | Invalid Request |
|  12 |          R9          |    1     |         1        |  1   |       1      |  luxury  |    minute   |      yes      |       54.6      |       54.6      |
|  13 |          R10         |    1     |         1        |  1   |       1      |  budget  |    minute   |      yes      |       14.7      |       14.7      |
|  14 |          R11         |    1     |         1        |  1   |       1      |  luxury  |    minute   |      no       |       54.6      |       54.6      |
|  14 |          R12         |    1     |         1        |  1   |       1      |  budget  |    minute   |      no       |       14.7      |       14.7      |
|  14 |          R13         |    1     |         1        |  1   |       1      |  budget  |  fixed_price |      yes      |       12.5      |       12.5      |
|  14 |          R14         |    1     |         1        |  1   |       1      |  budget  |  fixed_price |      no       |       12.5      |       12.5      |


